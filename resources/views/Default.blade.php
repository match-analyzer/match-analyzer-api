<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Upload file - MatchAnalyser</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

    <main>
        <div class="container">
            <div class="row">
                <div id="app_container" style="width:100%;" data-resultUrl="{{ $resultsUrl }}">
                <div class="col-lg-6 offset-lg-3 col-md-12 py-5" style="min-height:100vh;">
                        <h1>Please upload file</h1>
                        <form method="POST" action="{{ $uploadUrl }}" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="file">Choose your file</label>
                                <input name="file" type="file" class="form-control-file" id="file">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>

</body>
</html>
