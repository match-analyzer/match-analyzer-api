<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match', function(Blueprint $table) {
            $table->char('id',10);
            $table->index('id');
            $table->string('name');
            $table->string('country');
            $table->integer('matchday');
            $table->timestamps();
        });

        Schema::create('team', function(Blueprint $table) {
            $table->char('id', 10);
            $table->index('id');

            $table->string('name');
            $table->string('country');

            $table->timestamps();
        });

        Schema::create('player', function(Blueprint $table) {
            $table->char('id', 10);
            $table->index('id');

            $table->string('first_name');
            $table->string('last_name');

            $table->char('team_id', 10);
            $table->foreign('team_id')->references('id')->on('team');

            $table->timestamps();
        });

        Schema::create('match_player', function(Blueprint $table) {
            $table->increments('id');
            $table->index('id');

            $table->char('player_id',10);
            $table->foreign('player_id')->references('id')->on('player');

            $table->integer('shirtnumber');
            $table->string('position');
            $table->char('status', 10);

            $table->char('match_id', 10);
            $table->foreign('match_id')->references('id')->on('match');

            $table->timestamps();
        });

        Schema::create('player_statistics', function(Blueprint $table) {
            $table->string('name');
            $table->string('value');

            $table->unsignedInteger('match_player_id');
            $table->foreign('match_player_id')->references('id')->on('match_player');

            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match');
        Schema::drop('match_player');
        Schema::drop('player');
        Schema::drop('player_statistics');
        Schema::drop('team');
    }
}
