<?php

namespace App\Services;

use App\Models\Match;
use App\Models\MatchPlayer;
use App\Models\Player;
use App\Models\PlayerStatistics;
use App\Models\Team;
use Illuminate\Support\Facades\DB;

class MatchService {


    /**
     * @param string $id
     * @param string $name
     * @param string $country
     * @param string $matchDay
     * @return Match
     * @throws \Throwable
     */
    public function insertMatch(string $id, string $name, string $country, string $matchDay) : Match {
        $match = new Match([
            'id' => $id,
            'name' => $name,
            'country' => $country,
            'matchday' => $matchDay
        ]);
        $match->saveOrFail();
        return $match;
    }


    /**
     * @param string $id
     * @param string $name
     * @param string $country
     * @return Team
     */
    public function insertTeam(string $id, string $name, string $country) : Team {
        $team = new Team([
            'id' => $id,
            'name' => $name,
            'country' => $country
        ]);
        $team->saveOrFail();
        return $team;
    }

    /**
     * @param string $id
     * @param string $firstName
     * @param string $lastName
     * @param Team $team
     * @return Player
     */
    public function insertPlayer(string $id, string $firstName, string $lastName, Team $team) : Player {
        $player = new Player([
            'id' => $id,
            'first_name' => $firstName,
            'last_name' => $lastName,
        ]);
        $player->team()->associate($team);
        $player->saveOrFail();
        return $player;
    }

    /**
     * @param string $playerId
     * @param string $position
     * @param string $shirtnumber
     * @param string $status
     * @param string $matchId
     * @return MatchPlayer
     */
    public function insertMatchPlayer(string $playerId, string $position, string $shirtnumber, string $status, string $matchId) : MatchPlayer {
        $matchPlayer = new MatchPlayer([
            'player_id' => $playerId,
            'position' => $position,
            'shirtnumber' => $shirtnumber,
            'status' => $status,
            'match_id' => $matchId,
        ]);

        $matchPlayer->saveOrFail();
        return $matchPlayer;
    }

    /**
     * @param string $name
     * @param string $value
     * @param MatchPlayer $matchPlayer
     * @return PlayerStatistics
     * @throws \Throwable
     */
    public function insertPlayerStatistics(string $name, string $value, MatchPlayer $matchPlayer) {
        $playerStatistics = new PlayerStatistics([
            'name' => $name,
            'value' => $value
        ]);
        $playerStatistics->matchPlayer()->associate($matchPlayer);
        $playerStatistics->saveOrFail();
        return $playerStatistics;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function checkMatchExists(string $id) : bool {
        return boolval(Match::where('id', '=', $id)->count());
    }

    /**
     * @param string $statType
     * @return bool
     */
    public function checkStatTypeExists(string $statType) : bool {
        return boolval(PlayerStatistics::where('name', '=', $statType)->count());
    }

    /**
     * @param string $matchId
     * @param string $statType
     * @param integer $limit
     * @return mixed
     */
    public function getMatchPlayersStats(string $matchId, string $statType, int $limit) {
        return DB::table('match')
            ->select(['team.name as team', 'player.first_name', 'player.last_name', 'match_player.position', 'player_statistics.value'])
            ->join('match_player', 'match.id', '=', 'match_player.match_id')
            ->join('player', 'match_player.player_id', '=', 'player.id')
            ->join('team', 'team.id', '=', 'player.team_id')
            ->join('player_statistics', 'match_player.id', '=', 'player_statistics.match_player_id')
            ->where('match.id', $matchId)
            ->where('player_statistics.name', $statType)
            ->orderBy('player_statistics.value', 'DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * @param string $matchId
     * @param string $statType
     */
    public function getMatchTeamsStats(string $matchId, string $statType) {
        return DB::table('match')
            ->groupBy('team.name')
            ->selectRaw('team.name, SUM(player_statistics.value) as total')
            ->join('match_player', 'match.id', '=', 'match_player.match_id')
            ->join('player_statistics', 'match_player.id', '=', 'player_statistics.match_player_id')
            ->join('player', 'match_player.player_id', '=', 'player.id')
            ->join('team', 'team.id', '=', 'player.team_id')
            ->where('match.id', $matchId)
            ->where('player_statistics.name', $statType)
            ->get();
    }




}
