<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {

    protected $fillable = ["id", "name", "country"];

    protected $table = "team";

    protected $keyType = "char";

    public $incrementing = false;

    public static $rules = [
        "id" => "required",
        "name" => "required",
        "country" => "required",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players() {
        return $this->hasMany("App\Models\Player");
    }


}
