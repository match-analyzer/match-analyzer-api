<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model {

    protected $fillable = ["id", "name", "country", "matchday"];

    protected $table = "match";

    protected $keyType = "char";

    public $incrementing = false;


    public static $rules = [
        "id" => "required",
        "name" => "required",
        "country" => "required",
        "matchday" => "required",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchPlayers() {
        return $this->hasMany('App\Models\MatchPlayer');
    }


}
