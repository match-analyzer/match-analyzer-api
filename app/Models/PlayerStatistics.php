<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerStatistics extends Model {

    protected $fillable = ["name", "value", "match_player_id"];

    protected $table = "player_statistics";

    public static $rules = [
        "name" => "required",
        "value" => "required",
        "match_players_id" => "required",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function matchPlayer() {
        return $this->belongsTo("App\Models\MatchPlayer");
    }

}
