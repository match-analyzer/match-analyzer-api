<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchPlayer extends Model {

    protected $fillable = ["player_id", "position", "shirtnumber", "status", "match_id"];

    protected $table = "match_player";

    public static $rules = [
        "player_id" => "required",
        "position" => "required",
        "shirtnumber" => "required",
        "status" => "required",
        "match_id" => "match_id"
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player() {
        return $this->belongsTo("App\Models\Player");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match() {
        return $this->belongsTo("App\Models\Match");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playerStatistics() {
        return $this->hasMany('App\Models\PlayerStatistics');
    }

}
