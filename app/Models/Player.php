<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model {

    protected $fillable = ["id", "first_name", "last_name", "team_id"];

    protected $table = "player";

    protected $keyType = "char";

    public $incrementing = false;

    public static $rules = [
        "id" => "required",
        "first_name" => "required",
        "last_name" => "required",
        "team_id" => "required",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team() {
        return $this->belongsTo("App\Models\Team");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matchPlayers() {
        return $this->hasMany("App\Models\MatchPlayer");
    }

}
