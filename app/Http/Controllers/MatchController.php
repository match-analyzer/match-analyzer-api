<?php

namespace App\Http\Controllers;

use App\Models\Match;

use App\Services\MatchService;
use Illuminate\Http\Request;


class MatchController extends Controller
{

    /**
     * @var MatchService
     */
    protected $matchService;

    public function __construct(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    public function showStatsAction(Request $request, string $matchId, string $statType, int $limit) {

        if(!$this->matchService->checkStatTypeExists($statType)) {
            return response()->json(['status' => 'error', 'message' => 'Given type of statistics does not exists!']);
        }

        $playersStat = $this->matchService->getMatchPlayersStats($matchId, $statType, $limit);
        $teamsStat = $this->matchService->getMatchTeamsStats($matchId, $statType);

        return response()->json([
            'players' => $playersStat,
            'teams' => $teamsStat
        ]);
    }


}
