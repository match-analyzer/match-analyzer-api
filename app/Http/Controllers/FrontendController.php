<?php

namespace App\Http\Controllers;


class FrontendController extends Controller
{

    public function showAction() {
       return view('Default', ['uploadUrl' => url('/api/upload'), 'resultsUrl' => url('/api/match/{id}/stats/{type}')]);
    }

}
