<?php

namespace App\Http\Controllers;

use App\Models\Match;
use App\Models\MatchPlayer;
use App\Models\PlayerStatistics;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use App\Services\MatchService;

class UploadController extends Controller {

    /**
     * @var MatchService
     */
    protected $matchService;

    public function __construct(MatchService $matchService) {
        $this->matchService = $matchService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAction(Request $request) {

        try {
            $this->validate($request, [
                'file' => [
                    'required',
                    'mimetypes:application/xml,text/xml,text/plain'
                ]
            ]);

            $file = $request->file('file');
            $xml = simplexml_load_string($file->get());

            if(!isset($xml->SoccerDocument->Competition)) throw new \Exception('Invalid file structure!');

            $matchId = (string) $xml->SoccerDocument->Competition['uID'];

            if($this->matchService->checkMatchExists($matchId)) {
                return response()->json(
                    ['status' => 'warning', 'message' => 'Match already exists!', 'matchId' => $matchId],
                    Response::HTTP_CONFLICT
                );
            }

            DB::beginTransaction();

            $this->insertMatch($xml->SoccerDocument->Competition);
            $this->insertTeamsWithPlayers($xml->SoccerDocument->children()->Team);
            $this->insertMatchPlayersAndStats($xml->SoccerDocument->MatchData->children()->TeamData, $matchId);

            DB::commit();


            return response()->json(['status' => 'success', 'matchId' => $matchId]);

        } catch(\Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ],Response::HTTP_BAD_REQUEST);

        }

    }

    /**
     * @param \SimpleXMLElement $el
     * @return Match
     */
    protected function insertMatch(\SimpleXMLElement $el) : Match {
        return $this->matchService->insertMatch($el['uID'], $el->Country, $el->Name, $el->Stat[3]);
    }


    /**
     * @param \SimpleXMLElement $el
     * @return Team[]
     */
    protected function insertTeamsWithPlayers(\SimpleXMLElement $el) : array {
        $teams = [];
        foreach($el as $team) {
            $teams[] = $teamObj = $this->matchService->insertTeam($team['uID'], $team->Name, $team->Country);
            $this->insertPlayers($team->Player, $teamObj);
        }
        return $teams;
    }

    /**
     * @param \SimpleXMLElement $el
     * @param Team $team
     * @return Player[]
     */
    protected function insertPlayers(\SimpleXMLElement $el, Team $team) : array {
        $players = [];
        foreach($el as $player) {
            $players[] = $this->matchService->insertPlayer($player['uID'], $player->PersonName->First, $player->PersonName->Last, $team);
        }
        return $players;
    }

    /**
     * @param \SimpleXMLElement $el
     * @param string $matchId
     * @return MatchPlayer[]
     */
    protected function insertMatchPlayersAndStats(\SimpleXMLElement $el, string $matchId) : array {
        $matchPlayers = [];
        foreach($el as $team) {
            foreach($team->PlayerLineUp->children()->MatchPlayer as $matchPlayer) {
                $playerId = $matchPlayer['PlayerRef'];
                $matchPlayerObj = $this->matchService->insertMatchPlayer($playerId, $matchPlayer['Position'], $matchPlayer['ShirtNumber'], $matchPlayer['Status'], $matchId);
                $matchPlayers[] = $matchPlayerObj;
                $this->insertPlayerStatistics($matchPlayer->children()->Stat, $matchPlayerObj);
            }
        }
        return $matchPlayers;
    }

    /**
     * @param \SimpleXMLElement $el
     * @param MatchPlayer $matchPlayer
     * @return PlayerStatistics[]
     * @throws \Throwable
     */
    protected function insertPlayerStatistics(\SimpleXMLElement $el, MatchPlayer $matchPlayer) : array {
        $playerStatistics = [];
        foreach($el as $stat) {
            $playerStatistics[] = $this->matchService->insertPlayerStatistics($stat['Type'], $stat, $matchPlayer);
        }
        return $playerStatistics;
    }


}
