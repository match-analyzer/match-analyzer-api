# Match analyser API

App analyses match document, saves data to dabase and generate list of statistics.

## Prerequirements
To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

## Build & run containers
Build image `docker-compose build`

Run containers `docker-compose up`


## Usage
The app will be available at `http://localhost:8010`. 
You can change the IP, port in `docker-compose.yaml` file

### Upload match document

Request:
```yaml
Method: POST
Path: /api/upload
Body: multipart/form-data
Parameters:
  file: 
  - name: file
  - type: file
```

Response:
```json
{
    "status": "success",
    "matchId": "XX-YY-ZZ"
}
```

#### or just use upload form available at `/frontend`

### Get match statistics

Request:
```yaml
Method: GET
Path: /api/match/{matchId}/stats/{statType}/{limit}
Parameters:
  matchId: #match identifier from /api/upload response
  - name: matchId
  - type: string | mixed
  
  statType: # for ex. `leftside_pass`, `accurate_pass`, `touches`
  - name: statType
  - type: string
  
  limit: #max number of players in result
  - name: limit
  - type: integer
  
```

Response:
```json
{
    "players": [
        {
            "team": "Swansea City",
            "first_name": "Jordan",
            "last_name": "Ayew",
            "position": "Striker",
            "value": 7
        },
       
        {
            ...
        }
    ],
    "teams": [
        {
            "name": "Everton",
            "total": 19
        },
        {
            "name": "Swansea City",
            "total": 28
        }
    ]
}
```
